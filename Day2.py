#!/usr/bin/python3
import re

f = open("passwords.txt")
passwords = f.readlines()
validCount = 0

for password in passwords:
    passwordGrouping = re.search("([0-9]+)-([0-9]+) ([a-zA-Z]): (.*)", password)

    position1 = int(passwordGrouping.group(1))-1
    position2 = int(passwordGrouping.group(2))-1
    requirement = passwordGrouping.group(3)
    actualPassword = passwordGrouping.group(4)

    if actualPassword[position1] == requirement and not actualPassword[position2] == requirement:
        validCount += 1
    elif not actualPassword[position1] == requirement and actualPassword[position2] == requirement:
        validCount += 1

print(validCount)