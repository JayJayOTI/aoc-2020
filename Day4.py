#!bin/python3
import re

f = open("passports.txt")
passports = f.readlines()

passportString = ""
validCounter = 0
validityCounter = 0
keyValueRegex = re.compile("([a-zA-Z]{3}):(.*)")
byrRegex = re.compile("^(19[2-9][0-9]|200[0-2])$")
iyrRegex = re.compile("^(201[0-9]|2020)$")
eyrRegex = re.compile("^(202[0-9]|2030)$")
hgtRegex = re.compile("^(1([5-8][0-9]|9[0-3])cm|(59|6[0-9]|7[0-6])in)$")
hairRegex = re.compile("^[#][a-f0-9]{6}$")
eyeRegex = re.compile("^(amb|blu|brn|gry|grn|hzl|oth){1}$")
pidRegex = re.compile("^[0-9]{9}$")


for i in range(len(passports)):
    if not passports[i].isspace():
        passportString = passportString + " " + passports[i]
        if i == len(passports)-1:
            passportArray = passportString.split()
            if len(passportArray) == 8 or (len(passportArray) == 7 and passportString.find("cid") == -1):
                for passAttribute in passportArray:
                    keyValue = re.search(keyValueRegex, passAttribute)
                    if keyValue.group(1) == "byr" and byrRegex.match(keyValue.group(2)):
                        validityCounter += 1
                    elif keyValue.group(1) == "iyr" and iyrRegex.match(keyValue.group(2)):
                        validityCounter += 1
                    elif keyValue.group(1) == "eyr" and eyrRegex.match(keyValue.group(2)):
                        validityCounter += 1
                    elif keyValue.group(1) == "hgt" and hgtRegex.match(keyValue.group(2)):
                        validityCounter += 1
                    elif keyValue.group(1) == "hcl" and hairRegex.match(keyValue.group(2)):
                        validityCounter += 1
                    elif keyValue.group(1) == "ecl" and eyeRegex.match(keyValue.group(2)):
                        validityCounter += 1
                    elif keyValue.group(1) == "pid" and pidRegex.match(keyValue.group(2)):
                        validityCounter += 1
                if validityCounter == 7:
                    validCounter += 1
                validityCounter = 0
    else:
        passportArray = passportString.split()
        if len(passportArray) == 8 or (len(passportArray) == 7 and passportString.find("cid") == -1):
            for passAttribute in passportArray:
                keyValue = re.search(keyValueRegex, passAttribute)
                if keyValue.group(1) == "byr" and byrRegex.match(keyValue.group(2)):
                    validityCounter += 1
                elif keyValue.group(1) == "iyr" and iyrRegex.match(keyValue.group(2)):
                    validityCounter += 1
                elif keyValue.group(1) == "eyr" and eyrRegex.match(keyValue.group(2)):
                    validityCounter += 1
                elif keyValue.group(1) == "hgt" and hgtRegex.match(keyValue.group(2)):
                    validityCounter += 1
                elif keyValue.group(1) == "hcl" and hairRegex.match(keyValue.group(2)):
                    validityCounter += 1
                elif keyValue.group(1) == "ecl" and eyeRegex.match(keyValue.group(2)):
                    validityCounter += 1
                elif keyValue.group(1) == "pid" and pidRegex.match(keyValue.group(2)):
                    validityCounter += 1
                #else:
                    #print(passAttribute)
            if validityCounter == 7:
                validCounter += 1
            validityCounter = 0
        passportString = ""

print(validCounter)