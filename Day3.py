#!/usr/bin/python

f = open("treeMap.txt", "r")
treeMap = f.readlines()

x1 = 0
right1down1 = 0
x3 = 0
right3down1 = 0
x5 = 0
right5down1 = 0
x7 = 0
right7down1 = 0
right1down2 = 0

basis = len(treeMap[0]) - 1
print(basis)

for treeLine in treeMap:
    if treeLine[x1] == "#":
        right1down1 += 1
    if treeLine[x3] == "#":
        right3down1 += 1
    if treeLine[x5] == "#":
        right5down1 += 1
    if treeLine[x7] == "#":
        right7down1 += 1
    x1 = (x1+1) % basis
    x3 = (x3+3) % basis
    x5 = (x5+5) % basis
    x7 = (x7+7) % basis

y = 0
x = 0
while y < len(treeMap):
    if treeMap[y][x] == "#":
        right1down2 += 1
    x = (x+1) % basis
    y += 2

print("Trees per slope: " + str(right1down1) + ", " + str(right1down2) + ", " + str(right3down1) + ", " + str(right5down1) + ", " + str(right7down1))
print("Total amount of trees: " + str(right1down1+right1down2+right3down1+right5down1+right7down1))
print("Tree product: " + str(right1down1*right1down2*right3down1*right5down1*right7down1))